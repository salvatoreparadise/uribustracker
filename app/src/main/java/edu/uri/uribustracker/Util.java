package edu.uri.uribustracker;

/**
 * Created by ray on 4/25/16.
 */
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import com.google.android.gms.maps.model.LatLng;
public class Util {
    public static <T> HashSet<T> setCopy(Set<T> s) {
        HashSet<T> retval = new HashSet<T>();
        for (Iterator<T> it = s.iterator(); it.hasNext(); ) {
            retval.add(it.next());
        }
        return retval;
    }
    public static void printLatLong(LatLng l) {
        System.out.print("{ ");
        System.out.print(l.latitude);
        System.out.print(", ");
        System.out.print(l.longitude);
        System.out.println(" }");
    }
}
