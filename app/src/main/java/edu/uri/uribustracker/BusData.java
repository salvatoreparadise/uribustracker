package edu.uri.uribustracker;

/**
 * Created by ray on 4/24/16.
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashSet;

public class BusData {
    public double latitude;
    public double longitude;
    public String time;
    public String route;
    public String vehicle;
    static public HashSet<String> pluckRoutes(ArrayList<BusData> bd) {
        HashSet<String> vehicles = new HashSet<String>();
        for (Iterator<BusData> it = bd.iterator(); it.hasNext();) {
            vehicles.add(it.next().vehicle);
        }
        return vehicles;
    }
}